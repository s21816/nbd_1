package example

import scala.annotation.tailrec

object Hello extends Greeting with App {
  punkt1_a(weekdays)
  punkt1_b(weekdays)
  punkt1_c(weekdays)
  println(punkt2_a(weekdays))
  println(punkt2_b(weekdays))
  punkt3_a(weekdays)
  punkt4_a(weekdays)
  punkt4_b(weekdays)
  punkt4_c(weekdays) 
  println(punkt5_a(produkty))
  punkt6_a(2,"dds",1.2)
  punkt7_a(produkty,"kaszkiet")
  println(punkt8_a(numbers_with_zoro))
  println(punkt9_a(numbers_with_zoro))
  println(punkt10_a(numbers_with_zoro))
}

trait Greeting {
  // lazy val greeting: String = "hello"  
  val weekdays: List[String] = List("Poniedziałek","Wtorek","Środa","Czwartek","Piątek","Sobota","Niedziela")
  val produkty: Map[String,Double] = Map("kaszkiet" -> 34.0, "binda" -> 18.0, "lufka" -> 8.0, "tabakiera" -> 18.0, "gorset" -> 50.0)
  val numbers_with_zoro: List[Int] = List(2,9,0,51,-4,0,-1,-17,23,0,-6,3)


  // ZADANIE PIERWSZE
  def punkt1_a (lista: List[String]) = {
    var resultstring: String = ""
    for (day <- lista){
      if (resultstring.isEmpty()) resultstring = day
      else  resultstring = resultstring + "," + day
    }
    println(resultstring)
  }

  def punkt1_b (lista: List[String]) = {
    var resultstring: String = ""
    for (day <- lista){
      if (day(0) == 'P'){
        if (resultstring.isEmpty()) resultstring = day
        else  resultstring = resultstring + "," + day
      }
    }
    println(resultstring)
  }

  def punkt1_c (lista: List[String]) = {
    var resultstring: String = ""
    var i = 0
    while (i < lista.size){
      if (i == 0) resultstring = lista(0)
      else  resultstring = resultstring + "," + lista(i)
      i += 1
    }
    println(resultstring)
  }


  // ZADANIE DRUGIE
  def punkt2_a (lista: List[String], i: Int = 0): String = {
    if (i < lista.size -1 )  lista(i) + "," + punkt2_a(lista, i + 1)
    else lista(i)
  }

  def punkt2_b (lista: List[String], i: Int = 1): String = {
    if (i < lista.size )  lista(lista.size - i) + "," + punkt2_b(lista, i + 1)
    else lista(0)
  }


  //  ZADANIE TRZECIE
  @tailrec
  final def punkt3_a (lista: List[String], inputstring: String = "", inputint: Int = 0){
    var resultstring: String = inputstring
    var i: Int = inputint
    if (i == lista.size){
      println(resultstring)
    }else{
      if (i == 0) resultstring = lista(0)
      else  resultstring = resultstring + "," + lista(i)
      i += 1
      punkt3_a(lista,resultstring,i)
    }
  }

  //ZADANIE CZWARTE
  def punkt4_a(lista: List[String]) {
    println(
      lista.foldLeft("") { (acc, i) =>
        if (acc.isEmpty()) acc + i
        else acc + "," + i
      }
    )
  }

  def punkt4_b(lista: List[String]) {
    println(
        lista.foldRight("") { (acc, i) =>
        if (i.isEmpty()) acc + i
        else acc + "," + i
      }
    )  
  }

  def punkt4_c(lista: List[String]) {
    println(
        lista.fold("") { (acc, i) =>
          if (i(0) == 'P' && acc.isEmpty()) acc + i
          else if (i(0) == 'P') acc + "," + i
          else acc
      }
    )  
  }

  // ZADANIE PIĄTE
  def punkt5_a(produkty: Map[String,Double]): Map[String,Double] = {
    produkty.transform((k,v) => Math.round(v*90.0)/100.0)
  }

  // ZADANIE SZÓSTE
  def punkt6_a(t: (Any,Any,Any)){
    if (t._1.getClass() != t._2.getClass() && t._2.getClass() != t._3.getClass() && t._1.getClass() != t._3.getClass()) {
      println(t._1, t._2, t._3)    
    }
  }

  //ZADANIE SIÓDME
  def punkt7_a(produkty: Map[String,Double], klucz: String){
    punkt7_option(produkty, klucz) match {
      case Some(value) => println(s"istnieje wartość dla podanego klucza =  $value")
      case None => println("nie istnieje podany klucz")
    }
  }

  def punkt7_option(produkty: Map[String,Double], klucz: String): Option[Double] = {
    if (produkty.contains(klucz)) Some(produkty(klucz))
    else None
  }

  //ZADANIE ÓSME
  

  def punkt8_a(lista1: List[Int], lista2: List[Int] = List(), i: Int = 0): List[Int] = {
    if (lista1(i) == 0 && i < lista1.size -1 )  punkt8_a(lista1, lista2, i+1)
    else if (lista1(i) != 0 && i < lista1.size -1)   punkt8_a(lista1, lista2 :+ lista1(i), i+1)
    else lista2
  }

  //ZADANIE DZIEWIĄTE

  def punkt9_a(lista: List[Int]): List[Int] = {
    lista.map(x => x + 1)
  }

  //ZADANIE DZISIĄTE
  
  def punkt10_a(lista: List[Int]): List[Int] = {
    lista.filter(_ > -6)
    .filter(_ < 13 )
    .map(x => (x).abs)
  }
}
